<?php
/*
 * Plugin Name:       WooCommerce Liisi gateway
 * Plugin URI:        https://prelive.liisi.ee/static/payment_link_doc/
 * Description:       Liisi payment gateway for WooCommerce.
 * Version:           0.1.7
 * Author:            Veebipoed and Liisi
 * Author URI:        http://www.liisi.ee/
 * Text Domain:       liisi
 * Domain Path:       languages
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'WC_Liisi' ) ) {

    final class WC_Liisi {

        protected static $instance = null;

        public $version = '0.1.7';
        public $text_domain = 'liisi';
        public $name = 'Gateway Liisi';

        private function __construct() {

            add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'action_links' ) );
            add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

            if ( !is_plugin_active( 'woocommerce/woocommerce.php')) {
                add_action('admin_notices', array( $this, 'woocommerce_missing_notice' ) );
                return false;
            } else {
                if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.2', '>=' ) ) {
                    if ( class_exists( 'WC_Payment_Gateway' ) ) {
                        $this->includes();
                        add_filter( 'woocommerce_payment_gateways', array( $this, 'add_gateway' ) );
                    }
                } else {
                    add_action( 'admin_notices', array( $this, 'upgrade_notice' ) );
                    return false;
                }
            }
        }

        public function __clone() {
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'liisi' ), $this->version );
        }

        public function __wakeup() {
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'liisi' ), $this->version );
        }

        public static function get_instance() {

            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        private function includes() {
            require_once 'includes/class-wc-gateway-liisi.php';
            require_once 'includes/class-liisi-order.php';
            require_once 'includes/class-liisi-api.php';
        }

        public function action_links( $links ) {
            if ( current_user_can( 'manage_woocommerce' ) ) {
                $plugin_links = array(
                    '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_liisi' ) . '">' . __( 'Payment Settings', 'liisi' ) . '</a>',
                );

                return array_merge( $plugin_links, $links );
            }

            return $links;
        }

        public function load_plugin_textdomain() {
            $lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
            $lang_dir = apply_filters( 'woocommerce_liisi_languages_directory', $lang_dir );

            $locale = apply_filters( 'plugin_locale',  get_locale(), 'liisi' );
            $mofile = sprintf( '%1$s-%2$s.mo', 'liisi', $locale );

            $mofile_local  = $lang_dir . $mofile;
            $mofile_global = WP_LANG_DIR . '/liisi/' . $mofile;

            if ( file_exists( $mofile_global ) ) {
                load_textdomain( 'liisi', $mofile_global );
            } else if ( file_exists( $mofile_local ) ) {
                load_textdomain( 'liisi', $mofile_local );
            } else {
                load_plugin_textdomain( 'liisi', false, $lang_dir );
            }
        }

        public function woocommerce_missing_notice() {
             echo '<div class="error woocommerce-message wc-connect"><p>'.
             sprintf(
                __(
                    'Sorry, <strong>WooCommerce %s</strong> requires WooCommerce to be installed and activated first. Please install <a href="%s">WooCommerce</a> first.',
                    'liisi'
                ),
                $this->name,
                admin_url('plugin-install.php?tab=search&type=term&s=WooCommerce' )
            ) . '</p></div>';
        }

        public function upgrade_notice() {
            echo '<div class="updated woocommerce-message wc-connect"><p>'.
            sprintf(
                __(
                    'WooCommerce %s depends on version 2.2 and up of WooCommerce for this gateway to work! Please upgrade before activating.',
                    'liisi'
                ), $this->name
            ) . '</p></div>';
        }

        public function add_gateway( $methods ) {
            $methods[] = 'WC_Gateway_Liisi';
            return $methods;
        }
    }

    // **********************************
    // *** Liisi logo on product page ***
    // **********************************
     function liisi_scripts_with_jquery() {
       wp_register_script( 'liisilogoscript', plugins_url( '/js/liisilogoupload.js', __FILE__ ),array( 'jquery'));
       wp_enqueue_script( 'liisilogoscript'); 
        // echo '************* '. plugins_url( '/js/liisilogoupload.js', __FILE__ );
    }
       function liisi_logo_on_product_page(){

        $liisi_class = new WC_Gateway_Liisi();
        $liisi_enabled  = $liisi_class->liisi_enabled_on_product_page;
        $product_id = get_the_ID();
        $product = wc_get_product( $product_id );
        //$product_price = $product->get_sale_price(); // цена на акцию если она установлен
        $product_price =  wc_get_price_including_tax($product); //конечная цена с налогом
        
        if ($liisi_enabled=='yes' && $product_price !='') {
            // Kontrollib
    
        $urlimage = $liisi_class->liisi_logo_pic;
        $intress  = $liisi_class->liisi_intress;
        $periood = $liisi_class->liisi_periood;
        $min_payment = $liisi_class->min_payment; //Minimaalne järelmaksu igakuise osamakse suurus on
        $r = $intress/100;
        $jarelmaks = ($product_price *(1+ $r * $periood/12))/$periood;
        $liisi_url  = $liisi_class->liisi_product_url;
        $currency_code = get_woocommerce_currency();
        $currency_symbol = get_woocommerce_currency_symbol();

        if ($jarelmaks < $min_payment) {
            $jarelmaks = $min_payment;
            $product_page_text = '&emsp;';
        }
        // Kui arvutuskäik jääb alla 7€, siis kuvatakse "Logo" alates 7€/kuus

        $jarelmaks_html =  wc_price($jarelmaks);
        if (!isset($product_page_text)) {
            $product_page_text  = sprintf($liisi_class->liisi_product_page_text, $jarelmaks_html);
        }
        $no_pic_text  = 'Liisi järelmaks';
        
        echo '<div class="liisi_logo">'; // css .liisi_logo img see more /assets/css/liisifront.css
        //.liisi_logo img {float:left;  height: 26px;  margin-right: 5px}

        if ($liisi_url) {
            if ($urlimage){
                echo ' <a href="'.$liisi_url.'" target="_blank" ><img src="'.$urlimage.'" alt="'.$no_pic_text.'"><b> '.$product_page_text.'</b></a>'; 
            } else {
                echo ' <a href="'.$liisi_url.'" target="_blank" >'.$no_pic_text.' <b> '.$product_page_text.'</b></a>'; 
            }
            } else {
                if ($urlimage){ 
                    echo ' <img src="'.$urlimage.'" alt="'.$no_pic_text.'"><b> '.$product_page_text.'</b>'; // lisame liisi logo tekstga koos!
                } else {
                     echo $no_pic_text.' <b> '.$product_page_text.'</b>'; // lisame liisi logo tekstga koos!
                }
            }
        echo '</div>';
        }
    }

    add_action( 'plugins_loaded', array( 'WC_Liisi', 'get_instance' ), 0 );  
    add_action( 'woocommerce_single_product_summary', 'liisi_logo_on_product_page', 6 );   
    
}
